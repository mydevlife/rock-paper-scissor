package com.lucas.rockpaperscissor.repos;

import com.lucas.rockpaperscissor.util.RoundResult;
import org.springframework.stereotype.Repository;

import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class GlocalStatisticsRepo {
    private final AtomicInteger totalRounds = new AtomicInteger(0);
    private final AtomicInteger totalWinsPlayerOne = new AtomicInteger(0);
    private final AtomicInteger totalWinsPlayerTwo = new AtomicInteger(0);
    private final AtomicInteger totalDraws = new AtomicInteger(0);

    public AtomicInteger getTotalRounds() {
        return totalRounds;
    }

    public AtomicInteger getTotalWinsPlayerOne() {
        return totalWinsPlayerOne;
    }

    public AtomicInteger getTotalWinsPlayerTwo() {
        return totalWinsPlayerTwo;
    }

    public AtomicInteger getTotalDraws() {
        return totalDraws;
    }

    public void updateGlobalStatistics(RoundResult roundResult) {
        if (roundResult == RoundResult.DRAW) {
            incrementTotalDraws();
        } else if (roundResult == RoundResult.PLAYER_1) {
            incrementTotalWinsPlayerOne();
        } else if (roundResult == RoundResult.PLAYER_2) {
            incrementTotalWinsPlayerTwo();
        }
    }

    private void incrementTotalWinsPlayerOne() {
        this.totalWinsPlayerOne.getAndIncrement();
        this.totalRounds.getAndIncrement();
    }

    private void incrementTotalWinsPlayerTwo() {
        this.totalWinsPlayerTwo.getAndIncrement();
        this.totalRounds.getAndIncrement();
    }

    private void incrementTotalDraws() {
        this.totalDraws.getAndIncrement();
        this.totalRounds.getAndIncrement();
    }
}
