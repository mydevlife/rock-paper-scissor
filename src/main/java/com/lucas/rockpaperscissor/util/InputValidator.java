package com.lucas.rockpaperscissor.util;

import org.springframework.stereotype.Component;

@Component
public class InputValidator {

    public InputValidator() {}

    public void checkNullInput(Object... obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Argument is null: " + obj.getClass().getName());
        }
    }
}
