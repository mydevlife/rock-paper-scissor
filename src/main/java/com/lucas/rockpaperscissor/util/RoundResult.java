package com.lucas.rockpaperscissor.util;

public enum RoundResult {
    DRAW("DRAW"),
    PLAYER_1("PLAYER 1"),
    PLAYER_2("PLAYER 2");

    private String value;

    RoundResult(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
