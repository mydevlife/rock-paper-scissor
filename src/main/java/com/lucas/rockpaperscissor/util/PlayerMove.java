package com.lucas.rockpaperscissor.util;

public enum PlayerMove {
    ROCK("ROCK"),
    PAPER("PAPER"),
    SCISSOR("SCISSOR");

    private String value;

    PlayerMove(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "PlayerMove{" +
                "value='" + value + '\'' +
                '}';
    }
}
