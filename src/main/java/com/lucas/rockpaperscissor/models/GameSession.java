package com.lucas.rockpaperscissor.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class GameSession {

    private Player playerOne;
    private Player playerTwo;
    private List<Round> rounds;
    private String id;

    public GameSession(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.rounds = new ArrayList<>();
        this.id = UUID.randomUUID().toString();
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public String getId() {
        return id;
    }

    public void addRounds(Round round){
        if(rounds == null) {
            rounds = new ArrayList<>();
        }
        rounds.add(round);
    }

    public int totalRounds(){
        if(rounds == null) {
            return 0;
        }
        return rounds.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameSession)) return false;
        GameSession game = (GameSession) o;
        return Objects.equals(getPlayerOne(), game.getPlayerOne()) &&
                Objects.equals(getPlayerTwo(), game.getPlayerTwo()) &&
                Objects.equals(getRounds(), game.getRounds()) &&
                Objects.equals(getId(), game.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlayerOne(), getPlayerTwo(), getRounds(), getId());
    }

    @Override
    public String toString() {
        return "GameSession{" +
                "playerOne=" + playerOne +
                ", playerTwo=" + playerTwo +
                ", rounds=" + rounds +
                ", id='" + id + '\'' +
                '}';
    }
}
