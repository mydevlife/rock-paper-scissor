package com.lucas.rockpaperscissor.models;

import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;

import java.util.Objects;

public class Player {
    private PlayerMoveStrategy moveStrategy;
    private String name;

    public Player(PlayerMoveStrategy moveStrategy, String name) {
        this.moveStrategy = moveStrategy;
        this.name = name;
    }

    public PlayerMoveStrategy getMoveStrategy() {
        return moveStrategy;
    }

    public void setMoveStrategy(PlayerMoveStrategy moveStrategy) {
        this.moveStrategy = moveStrategy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" +
                "moveStrategy=" + moveStrategy +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return Objects.equals(getMoveStrategy(), player.getMoveStrategy()) &&
                Objects.equals(getName(), player.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMoveStrategy(), getName());
    }
}
