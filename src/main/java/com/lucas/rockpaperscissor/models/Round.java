package com.lucas.rockpaperscissor.models;

import com.lucas.rockpaperscissor.util.PlayerMove;
import com.lucas.rockpaperscissor.util.RoundResult;

import java.util.Objects;
import java.util.UUID;

public class Round {

    private PlayerMove playerOneMove;
    private PlayerMove playerTwoMove;
    private RoundResult winner;
    private String id;

    public Round(PlayerMove playerOneMove, PlayerMove playerTwoMove) {
        this.playerOneMove = playerOneMove;
        this.playerTwoMove = playerTwoMove;
        this.id = UUID.randomUUID().toString();
    }

    public PlayerMove getPlayerOneMove() {
        return playerOneMove;
    }

    public PlayerMove getPlayerTwoMove() {
        return playerTwoMove;
    }

    public RoundResult getWinner() {
        return winner;
    }

    public void setWinner(RoundResult winner) {
        this.winner = winner;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Round)) return false;
        Round round = (Round) o;
        return getPlayerOneMove() == round.getPlayerOneMove() &&
                getPlayerTwoMove() == round.getPlayerTwoMove() &&
                Objects.equals(getWinner(), round.getWinner()) &&
                Objects.equals(getId(), round.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlayerOneMove(), getPlayerTwoMove(), getWinner(), getId());
    }

    @Override
    public String toString() {
        return "Round{" +
                "playerOneMove=" + playerOneMove +
                ", playerTwoMove=" + playerTwoMove +
                ", winner=" + winner +
                ", id='" + id + '\'' +
                '}';
    }
}
