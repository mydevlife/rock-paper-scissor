package com.lucas.rockpaperscissor.controllers;

import org.springframework.ui.Model;


public interface GameController {

    public String startNewGame(Model model);
    public String playNewRound(Model model);
}
