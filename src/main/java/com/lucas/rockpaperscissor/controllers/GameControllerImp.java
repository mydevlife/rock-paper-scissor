package com.lucas.rockpaperscissor.controllers;

import com.lucas.rockpaperscissor.models.GameSession;
import com.lucas.rockpaperscissor.services.GameService;
import com.lucas.rockpaperscissor.services.GlocalStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

@Controller
@RequestMapping("/")
@Scope("session")
public class GameControllerImp implements GameController {

    private GameService gameService;

    private GlocalStatisticsService glocalStatisticsService;

    private GameSession gameSession;

    private String userId;

    @Autowired
    public GameControllerImp(GameService gameService, GlocalStatisticsService glocalStatisticsService) {
        this.gameService = gameService;
        this.glocalStatisticsService = glocalStatisticsService;
        this.userId = RequestContextHolder.currentRequestAttributes().getSessionId();
    }

    @Override
    @GetMapping
    public String startNewGame(Model model) {
        gameSession = gameService.start();
        model.addAttribute("rounds", gameSession.getRounds());
        model.addAttribute("totalrounds", gameSession.totalRounds());
        model.addAttribute("userId", userId);
        return "index";
    }

    @Override
    @GetMapping("/play")
    public String playNewRound( Model model) {
        gameService.playNewRound(gameSession);
        model.addAttribute("rounds", gameSession.getRounds());
        model.addAttribute("totalrounds", gameSession.totalRounds());
        model.addAttribute("userId", userId);
        return "index";
    }

    @GetMapping("/statistics")
    public String statistics(Model model) {
        model.addAttribute("totalRounds", glocalStatisticsService.getTotalRounds());
        model.addAttribute("totalWinsPlayerOne", glocalStatisticsService.getTotalWinsPlayerOne());
        model.addAttribute("totalWinsPlayerTwo", glocalStatisticsService.getTotalWinsPlayerTwo());
        model.addAttribute("totalDraws", glocalStatisticsService.getTotalDraws());
        return "statistics";
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleException(Model model) {
        return "error";
    }

}
