package com.lucas.rockpaperscissor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class RockPaperScissorApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockPaperScissorApplication.class, args);
	}

}
