package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.models.GameSession;
import com.lucas.rockpaperscissor.util.RoundResult;

public interface GameService {

    public GameSession start();
    public RoundResult playNewRound(GameSession game);
}
