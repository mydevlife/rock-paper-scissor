package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.util.PlayerMove;
import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;

public interface PlayerService {

    public Player createPlayer(PlayerMoveStrategy strategy, String name);
    public PlayerMove getMove(Player player) throws Exception;
}
