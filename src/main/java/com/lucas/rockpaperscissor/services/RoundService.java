package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.models.Round;
import com.lucas.rockpaperscissor.util.RoundResult;

public interface RoundService {

    public Round startNewRound(Player playerOne, Player playerTwo) throws Exception;
    public RoundResult getResult(Player playerOne, Player playerTwo, Round round);
}
