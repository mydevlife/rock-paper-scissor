package com.lucas.rockpaperscissor.services.imp;

import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.services.PlayerService;
import com.lucas.rockpaperscissor.services.RoundService;
import com.lucas.rockpaperscissor.util.PlayerMove;
import com.lucas.rockpaperscissor.models.Round;
import com.lucas.rockpaperscissor.util.RoundResult;
import com.lucas.rockpaperscissor.util.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoundServiceImp implements RoundService {

    private InputValidator inputValidator;

    private PlayerService playerService;

    @Autowired
    public RoundServiceImp(PlayerService playerService, InputValidator inputValidator) {
        this.playerService = playerService;
        this.inputValidator = inputValidator;
    }

    @Override
    public Round startNewRound(Player playerOne, Player playerTwo) throws Exception {
        inputValidator.checkNullInput(playerOne, playerTwo);
        PlayerMove playerOneMove = playerService.getMove(playerOne);
        PlayerMove playerTwoMove = playerService.getMove(playerTwo);
        return new Round(playerOneMove, playerTwoMove);
    }

    @Override
    public RoundResult getResult(Player playerOne, Player playerTwo, Round round) {
        inputValidator.checkNullInput(playerOne, playerTwo, round);

        PlayerMove playerMoveOne = round.getPlayerOneMove();
        PlayerMove playerMoveTwo = round.getPlayerTwoMove();

        if (playerMoveOne == playerMoveTwo) {
            round.setWinner(RoundResult.DRAW);
            return RoundResult.DRAW;
        }
        RoundResult result = calculateResult(playerMoveOne, playerMoveTwo);
        round.setWinner(result);
        return result;
    }

    private RoundResult calculateResult(PlayerMove playerMoveOne, PlayerMove playerMoveTwo) {
        switch (playerMoveOne) {
            case ROCK:
                return playerMoveTwo.equals(PlayerMove.SCISSOR) ? RoundResult.PLAYER_1 : RoundResult.PLAYER_2;
            case SCISSOR:
                return playerMoveTwo.equals(PlayerMove.PAPER) ? RoundResult.PLAYER_1 : RoundResult.PLAYER_2;
            case PAPER:
                return playerMoveTwo.equals(PlayerMove.ROCK) ? RoundResult.PLAYER_1 : RoundResult.PLAYER_2;
            default:
                throw new IllegalArgumentException("One of the moves is invalid: " + playerMoveOne.getValue());
        }
    }


}
