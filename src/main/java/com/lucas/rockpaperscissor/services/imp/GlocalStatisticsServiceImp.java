package com.lucas.rockpaperscissor.services.imp;

import com.lucas.rockpaperscissor.services.GlocalStatisticsService;
import com.lucas.rockpaperscissor.util.RoundResult;
import com.lucas.rockpaperscissor.repos.GlocalStatisticsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class GlocalStatisticsServiceImp implements GlocalStatisticsService {

    private GlocalStatisticsRepo repo;

    @Autowired
    public GlocalStatisticsServiceImp(GlocalStatisticsRepo repo) {
        this.repo = repo;
    }

    @Override
    public void updateGlobalStatistics(RoundResult roundResult) {
        repo.updateGlobalStatistics(roundResult);
    }

    @Override
    public AtomicInteger getTotalRounds() {
        return repo.getTotalRounds();
    }

    @Override
    public AtomicInteger getTotalWinsPlayerOne() {
        return repo.getTotalWinsPlayerOne();
    }

    @Override
    public AtomicInteger getTotalWinsPlayerTwo() {
        return repo.getTotalWinsPlayerTwo();
    }

    @Override
    public AtomicInteger getTotalDraws() {
        return repo.getTotalDraws();
    }
}
