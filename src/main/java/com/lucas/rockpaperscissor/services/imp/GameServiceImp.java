package com.lucas.rockpaperscissor.services.imp;

import com.lucas.rockpaperscissor.controllers.GameControllerImp;
import com.lucas.rockpaperscissor.models.GameSession;
import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.services.GameService;
import com.lucas.rockpaperscissor.services.PlayerService;
import com.lucas.rockpaperscissor.services.RoundService;
import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;
import com.lucas.rockpaperscissor.models.Round;
import com.lucas.rockpaperscissor.util.RoundResult;
import com.lucas.rockpaperscissor.services.GlocalStatisticsService;
import com.lucas.rockpaperscissor.util.InputValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImp implements GameService {

    Logger LOG = LoggerFactory.getLogger(GameControllerImp.class);

    private InputValidator inputValidator;

    private PlayerService playerService;

    private RoundService roundService;

    private GlocalStatisticsService glocalStatisticsService;


    @Autowired
    public GameServiceImp(PlayerService playerService, RoundService roundService, GlocalStatisticsService glocalStatisticsService, InputValidator inputValidator) {
        this.playerService = playerService;
        this.roundService = roundService;
        this.glocalStatisticsService = glocalStatisticsService;
        this.inputValidator = inputValidator;
    }

    @Override
    public GameSession start() {
        Player playerOne = playerService.createPlayer(PlayerMoveStrategy.ROCK, "Player_1_rock");
        Player playerTwo = playerService.createPlayer(PlayerMoveStrategy.RANDOM, "Player_2_random");
        inputValidator.checkNullInput(playerOne, playerTwo);
        return new GameSession(playerOne, playerTwo);
    }

    @Override
    public RoundResult playNewRound(GameSession gameSession) {
        inputValidator.checkNullInput(gameSession);
        RoundResult roundResult = null;
        try {
            Round round = roundService.startNewRound(gameSession.getPlayerOne(), gameSession.getPlayerTwo());
            roundResult = roundService.getResult(gameSession.getPlayerOne(), gameSession.getPlayerTwo(), round);
            glocalStatisticsService.updateGlobalStatistics(roundResult);
            gameSession.addRounds(round);
        } catch (Exception e) {
            LOG.error("Couldn't start new round!", e.getStackTrace());
        }
        return roundResult;
    }
}
