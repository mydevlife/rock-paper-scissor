package com.lucas.rockpaperscissor.services.imp;

import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.services.PlayerService;
import com.lucas.rockpaperscissor.util.PlayerMove;
import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;
import com.lucas.rockpaperscissor.util.InputValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class PlayerServiceImp implements PlayerService {

    private final Logger LOG = LoggerFactory.getLogger(PlayerServiceImp.class);

    @Autowired
    private InputValidator inputValidator;

    private final List<PlayerMove> moves = Arrays.asList(PlayerMove.values());
    private final Random random = new Random();

    @Override
    public Player createPlayer(PlayerMoveStrategy strategy, String name) {
        inputValidator.checkNullInput(strategy, name);
        return new Player(strategy, name);
    }

    @Override
    public PlayerMove getMove(Player player) {
        inputValidator.checkNullInput(player);
        if(player.getMoveStrategy() == PlayerMoveStrategy.ROCK) {
            return PlayerMove.ROCK;
        }else{
            return randomMove();
        }
    }

    private PlayerMove randomMove() {
        return moves.get(random.nextInt(moves.size()));
    }

}
