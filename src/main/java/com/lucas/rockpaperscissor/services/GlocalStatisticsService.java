package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.util.RoundResult;

import java.util.concurrent.atomic.AtomicInteger;

public interface GlocalStatisticsService {

    public void updateGlobalStatistics(RoundResult roundResult);
    public AtomicInteger getTotalRounds();
    public AtomicInteger getTotalWinsPlayerOne();
    public AtomicInteger getTotalWinsPlayerTwo();
    public AtomicInteger getTotalDraws();

}
