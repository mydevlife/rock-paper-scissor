package com.lucas.rockpaperscissor.controllers;

import com.lucas.rockpaperscissor.controllers.GameControllerImp;
import com.lucas.rockpaperscissor.models.GameSession;
import com.lucas.rockpaperscissor.services.GameService;
import com.lucas.rockpaperscissor.services.GlocalStatisticsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameControllerImp.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
public class GameControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameService gameService;

    @MockBean
    private GlocalStatisticsService glocalStatisticsService;

    @Mock
    private GameSession gameSession;

    @Test
    public void startGameTest() throws Exception {
        Mockito.doReturn(gameSession).when(gameService).start();
        Mockito.doReturn(Arrays.asList()).when(gameSession).getRounds();
        mvc.perform(get("/")).andExpect(status().isOk());
    }

    @Test
    public void statisticsTest() throws Exception {
        mvc.perform(get("/statistics")).andExpect(status().isOk());
    }

    @Test
    public void errorPlayBeforeStartGameTest() throws Exception {
        mvc.perform(get("/play")).andExpect(status().isBadRequest());
    }

    @Test
    public void errorHandlingTest() throws Exception {
        mvc.perform(get("/kjaskajskajsaksjakjs")).andExpect(status().isNotFound());
    }
}