package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.util.PlayerMove;
import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;
import com.lucas.rockpaperscissor.services.imp.PlayerServiceImp;
import com.lucas.rockpaperscissor.util.InputValidator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceTest {

    @InjectMocks
    private PlayerServiceImp playerService;

    @Mock
    private InputValidator validator;

    private static Player playerRock;
    private static Player playerRandom;

    @BeforeClass
    public static void setUp() {
        playerRock = new Player(PlayerMoveStrategy.ROCK, "playerOne");
        playerRandom = new Player(PlayerMoveStrategy.RANDOM, "playerRandom");
    }

    @Test
    public void createPlayerTest() {
        //When
        Player playerOne = playerService.createPlayer(PlayerMoveStrategy.ROCK, "playerOne");
        Player playerTwo = playerService.createPlayer(PlayerMoveStrategy.RANDOM, "playerTwo");

        //Then
        Assert.assertNotNull(playerOne);
        Assert.assertEquals(PlayerMoveStrategy.ROCK, playerOne.getMoveStrategy());
        Assert.assertNotNull(playerTwo);
        Assert.assertEquals(PlayerMoveStrategy.RANDOM, playerTwo.getMoveStrategy());
    }

    @Test
    public void getMoveTest() throws Exception {
        //When
        PlayerMove playerMoveOne = playerService.getMove(playerRock);
        PlayerMove playerMoveTwo = playerService.getMove(playerRandom);

        //Then
        Assert.assertNotNull(playerMoveOne);
        Assert.assertNotNull(playerMoveTwo);
        Assert.assertEquals(playerMoveOne, PlayerMove.ROCK);
        Assert.assertTrue(isValidMove(playerMoveTwo));
    }

    private boolean isValidMove(PlayerMove move) {
        return move == PlayerMove.ROCK
                || move == PlayerMove.PAPER
                || move == PlayerMove.SCISSOR;
    }

}
