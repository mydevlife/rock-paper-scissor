package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.util.RoundResult;
import com.lucas.rockpaperscissor.repos.GlocalStatisticsRepo;
import com.lucas.rockpaperscissor.services.imp.GlocalStatisticsServiceImp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.atomic.AtomicInteger;

@RunWith(MockitoJUnitRunner.class)
public class GlocalStatisticsServiceTest {

    @InjectMocks
    private GlocalStatisticsServiceImp glocalStatisticsServiceImp;

    @Mock
    private GlocalStatisticsRepo repo;

    private AtomicInteger totalRounds;
    private AtomicInteger totalWinsPlayerOne;
    private AtomicInteger totalWinsPlayerTwo;
    private AtomicInteger totalDraws;

    @Before
    public void setUp() {
        totalRounds = new AtomicInteger();
        totalWinsPlayerOne = new AtomicInteger();
        totalWinsPlayerTwo = new AtomicInteger();
        totalDraws = new AtomicInteger();

        Mockito.doReturn(totalRounds).when(repo).getTotalRounds();
        Mockito.doReturn(totalWinsPlayerOne).when(repo).getTotalWinsPlayerOne();
        Mockito.doReturn(totalWinsPlayerTwo).when(repo).getTotalWinsPlayerTwo();
        Mockito.doReturn(totalDraws).when(repo).getTotalDraws();
    }

    @Test
    public void initialStageTest() {
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalRounds().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalWinsPlayerOne().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalWinsPlayerTwo().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalDraws().get());
    }

    @Test
    public void updateDRAWTest() {
        glocalStatisticsServiceImp.updateGlobalStatistics(RoundResult.DRAW);
        totalRounds.incrementAndGet();
        totalDraws.incrementAndGet();
        Assert.assertEquals(1, glocalStatisticsServiceImp.getTotalRounds().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalWinsPlayerOne().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalWinsPlayerTwo().get());
        Assert.assertEquals(1, glocalStatisticsServiceImp.getTotalDraws().get());
    }

    @Test
    public void updatePLAYER1Test() {
        glocalStatisticsServiceImp.updateGlobalStatistics(RoundResult.PLAYER_1);
        totalRounds.incrementAndGet();
        totalWinsPlayerOne.incrementAndGet();

        Assert.assertEquals(1, glocalStatisticsServiceImp.getTotalRounds().get());
        Assert.assertEquals(1, glocalStatisticsServiceImp.getTotalWinsPlayerOne().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalWinsPlayerTwo().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalDraws().get());
    }

    @Test
    public void updatePLAYER2Test() {
        glocalStatisticsServiceImp.updateGlobalStatistics(RoundResult.PLAYER_1);
        totalRounds.incrementAndGet();
        totalWinsPlayerTwo.incrementAndGet();

        Assert.assertEquals(1, glocalStatisticsServiceImp.getTotalRounds().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalWinsPlayerOne().get());
        Assert.assertEquals(1, glocalStatisticsServiceImp.getTotalWinsPlayerTwo().get());
        Assert.assertEquals(0, glocalStatisticsServiceImp.getTotalDraws().get());
    }
}
