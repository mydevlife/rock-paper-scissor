package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.models.GameSession;
import com.lucas.rockpaperscissor.services.PlayerService;
import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;
import com.lucas.rockpaperscissor.services.RoundService;
import com.lucas.rockpaperscissor.util.RoundResult;
import com.lucas.rockpaperscissor.services.imp.GameServiceImp;
import com.lucas.rockpaperscissor.services.GlocalStatisticsService;
import com.lucas.rockpaperscissor.util.InputValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @InjectMocks
    private GameServiceImp gameService;

    @Mock
    private PlayerService playerService;

    @Mock
    private RoundService roundService;

    @Mock
    private InputValidator inputValidator;

    @Mock
    private GlocalStatisticsService glocalStatisticsService;

    @Test
    public void startGameTest(){
        //When
        GameSession game = gameService.start();

        //Then
        Assert.assertNotNull(game);
        Assert.assertTrue(game.getRounds().isEmpty());
        Mockito.verify(playerService).createPlayer(PlayerMoveStrategy.ROCK,"Player_1_rock");
        Mockito.verify(playerService).createPlayer(PlayerMoveStrategy.RANDOM,"Player_2_random");
    }

    @Test
    public void playNewRoundTest() throws Exception {
        //Given
        GameSession game = gameService.start();
        Assert.assertTrue(game.getRounds().isEmpty());

        //When
        RoundResult roundResult = gameService.playNewRound(game);
        Assert.assertNull(roundResult);
        Mockito.verify(roundService).startNewRound(game.getPlayerOne(), game.getPlayerTwo());
        Assert.assertEquals(1,game.getRounds().size());
    }
}
