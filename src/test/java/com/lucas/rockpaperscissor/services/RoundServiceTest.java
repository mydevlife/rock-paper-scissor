package com.lucas.rockpaperscissor.services;

import com.lucas.rockpaperscissor.models.Player;
import com.lucas.rockpaperscissor.services.PlayerService;
import com.lucas.rockpaperscissor.services.imp.RoundServiceImp;
import com.lucas.rockpaperscissor.util.PlayerMove;
import com.lucas.rockpaperscissor.util.PlayerMoveStrategy;
import com.lucas.rockpaperscissor.models.Round;
import com.lucas.rockpaperscissor.util.RoundResult;
import com.lucas.rockpaperscissor.util.InputValidator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RoundServiceTest {

    @InjectMocks
    private RoundServiceImp roundService;

    @Mock
    private PlayerService playerService;

    @Mock
    private InputValidator inputValidator;

    private static Player playerRock;
    private static Player playerRandom;

    @BeforeClass
    public static void setUp() {
        playerRock = new Player(PlayerMoveStrategy.ROCK, "PlayerOne");
        playerRandom = new Player(PlayerMoveStrategy.RANDOM, "PlayerTwo");
    }

    @Test
    public void startNewRoundTest() throws Exception {
        //When
        Mockito.when(playerService.getMove(playerRock)).thenReturn(PlayerMove.ROCK);
        Mockito.when(playerService.getMove(playerRandom)).thenReturn(PlayerMove.PAPER);
        Round round = roundService.startNewRound(playerRock, playerRandom);

        //Then
        Assert.assertNotNull(round);
        Assert.assertEquals(round.getPlayerOneMove(), playerService.getMove(playerRock));
        Assert.assertEquals(round.getPlayerTwoMove(), playerService.getMove(playerRandom));
    }

    @Test
    public void checkWinnerTest() throws Exception {
        //Given
        Mockito.when(playerService.getMove(playerRock)).thenReturn(PlayerMove.ROCK);
        Mockito.when(playerService.getMove(playerRandom)).thenReturn(PlayerMove.PAPER);
        PlayerMove playerRockMove = playerService.getMove(playerRock);
        PlayerMove playerRandomMove = playerService.getMove(playerRandom);
        Round round = new Round(playerRockMove, playerRandomMove);

        //when
        RoundResult winner = roundService.getResult(playerRock, playerRandom, round);

        //Then
        Assert.assertEquals(RoundResult.PLAYER_2, winner);
    }

}
