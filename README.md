**Rock Paper Scissor - Lottoland**

## Stack:
- SpringBoot
- Java 8
- Mockito
- Junit 4
- Thymelf
- BootStrap

## How to Run:
- from the project root folder (.../rock-paper-scissor/): `mvnw spring-boot:run`
- Go to: http://localhost:8080/

## Comments:
- Code was createded using TDD
- Two main [branches](https://gitlab.com/mydevlife/rock-paper-scissor/-/branches) where createded to implement part 1 and part 2
- To start a game as a new user you need to open a diferent browser or a use a diferent device.
- One user per HttpSession

## Screenshots
![image](/uploads/299fbbf171138a26ff444db3f04d1de9/image.png)
![image](/uploads/c33b04e64bcdedf9fa1755d013cd31e1/image.png)
![image](/uploads/4dc4f35442c7bdfe59f9d9e1564695b5/image.png)
